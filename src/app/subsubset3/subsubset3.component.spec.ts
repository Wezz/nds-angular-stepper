import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subsubset3Component } from './subsubset3.component';

describe('Subsubset3Component', () => {
  let component: Subsubset3Component;
  let fixture: ComponentFixture<Subsubset3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subsubset3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Subsubset3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
