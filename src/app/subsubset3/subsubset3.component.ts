import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subsubset3',
  templateUrl: './subsubset3.component.html',
  styleUrls: ['./subsubset3.component.scss']
})
export class Subsubset3Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  completedStep() {
    setTimeout(() => {
      const addCompleted = document.querySelectorAll('.nlsg-c-list-stepper__ol__li');
      [].forEach.call(addCompleted, (el, i) => {
        if (el.classList.contains('active')) {
          el.classList.add('step' + i);
          // add your own key here
          sessionStorage.step3 = 'completedStep' + i;
          el.classList.add('completed');
        }
      });
      // counter to update top nav
      const getCurrentStep = document.querySelector('.nlsg-c-list-stepper__ol__li.active');
      const currentStepContainer = document.getElementById('current-step');
      const currentStep = getCurrentStep.getAttribute('data-index');
      const total = parseFloat(currentStep) + 1;
      currentStepContainer.innerText = total.toString();
    }, 10);
  }

  stepperItem() {

    setTimeout(() => {
      const allActive = document.querySelectorAll('.active');
      [].forEach.call(allActive, (el) => {
          const last = allActive[(allActive.length - 1)];
          const total = last.getBoundingClientRect().top + last.getBoundingClientRect().height - 130;
          const progress: HTMLElement = document.getElementsByClassName('progress')[0] as HTMLElement;
          progress.style.height =  total  + 'px';
          sessionStorage.currentprogress = total;
      }, 100);
    });
  }

  checkValue(event) {
    const inputValue = event.target.value;
    const button = document.querySelector(".nlsg-c-button");

    if (inputValue) {
      button.removeAttribute("disabled");
    }
    else {
      button.setAttribute("disabled", "disabled");
    }
  }

}
