import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subsubset1Component } from './subsubset1.component';

describe('Subsubset1Component', () => {
  let component: Subsubset1Component;
  let fixture: ComponentFixture<Subsubset1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subsubset1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Subsubset1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
