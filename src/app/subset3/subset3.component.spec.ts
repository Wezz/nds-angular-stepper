import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subset3Component } from './subset3.component';

describe('Subset3Component', () => {
  let component: Subset3Component;
  let fixture: ComponentFixture<Subset3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subset3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Subset3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
