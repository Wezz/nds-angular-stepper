import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { Subset1Component } from './subset1/subset1.component';
import { Subset2Component } from './subset2/subset2.component';
import { Subsubset2Component } from './subsubset2/subsubset2.component';
import { Subsubset1Component } from './subsubset1/subsubset1.component';
import { Subsubset3Component } from './subsubset3/subsubset3.component';
import { Subset3Component } from './subset3/subset3.component';
import { AppComponent } from './app.component';
import { StepperModule } from 'stepper';
import { ButtonModule } from 'button';
import { InputModule } from 'input';
import { TooltipModule } from 'tooltip';
import { ModalModule } from 'modal';
import { StepperTemplateComponent } from './stepper-template/stepper-template.component';



@NgModule({
  declarations: [
    AppComponent,
    StepperTemplateComponent,
    Subset1Component,
    Subset2Component,
    Subsubset2Component,
    Subsubset1Component,
    Subsubset3Component,
    Subset3Component

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StepperModule,
    ButtonModule,
    InputModule,
    TooltipModule,
    ModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
