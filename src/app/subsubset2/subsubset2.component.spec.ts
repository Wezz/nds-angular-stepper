import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subsubset2Component } from './subsubset2.component';

describe('Subsubset2Component', () => {
  let component: Subsubset2Component;
  let fixture: ComponentFixture<Subsubset2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subsubset2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Subsubset2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
