import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StepperTemplateComponent } from './stepper-template/stepper-template.component';
import { Subset1Component } from './subset1/subset1.component';
import { Subset2Component } from './subset2/subset2.component';
import { Subset3Component } from './subset3/subset3.component';
import { Subsubset1Component } from './subsubset1/subsubset1.component';
import { Subsubset2Component } from './subsubset2/subsubset2.component';
import { Subsubset3Component } from './subsubset3/subsubset3.component';

const appRoutes: Routes = [
  { path: 'stepper-template', component: StepperTemplateComponent,
    children: [
      {
        path: 'sub-set-1',
        component: Subset1Component,
        outlet: 'currentStep',
      },
      {
        path: 'sub-sub-set-1',
        component: Subsubset1Component,
        outlet: 'currentStep'
      },
      {
        path: 'sub-set-2',
        component: Subset2Component,
        outlet: 'currentStep'
      },
      {
        path: 'sub-sub-set-2',
        component: Subsubset2Component,
        outlet: 'currentStep'
      },
      {
        path: 'sub-set-3',
        component: Subset3Component,
        outlet: 'currentStep'
      },
      {
        path: 'sub-sub-set-3',
        component: Subsubset3Component,
        outlet: 'currentStep'
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

