import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-stepper-template',
  templateUrl: './stepper-template.component.html',
  styleUrls: ['./stepper-template.component.scss']
})
export class StepperTemplateComponent implements OnInit {

  show: boolean;
  saveModalShown: boolean;
  cancelModalShown: boolean;

  constructor() {}

  listItems: any = [
  { mainStep: 'Step 1',
  index: 1,
  route: 'sub-set-1',
    sublist: [
        { substep: 'Sub set 1',
          route: 'sub-set-1',
          sublistItem: [
            { subsubstep: 'Sub-Sub set 1',  route: 'sub-sub-set-1'},
          ],
        }
    ],
  },
  { mainStep: 'Step 2',
    index: 2,
    route: 'sub-set-2',
    sublist: [
        { substep: 'Sub set 2',
          route: 'sub-set-2',
          sublistItem: [
            { subsubstep: 'Sub-Sub set 2' ,  route: 'sub-sub-set-2'},
          ],
        },
    ],
  },
  { mainStep: 'Step 3',
    index: 3,
    route: 'sub-set-3',
    sublist: [
        { substep: 'Sub set 3',
          route: 'sub-set-3',
          sublistItem: [
            { subsubstep: 'Sub-Sub set 3' ,  route: 'sub-sub-set-3'},
          ],
        },
    ],
  },
  { mainStep: 'Step 4',
    index: 4
  }
  ];

  ngOnInit() {}
}
