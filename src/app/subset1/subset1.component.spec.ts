import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subset1Component } from './subset1.component';

describe('Subset1Component', () => {
  let component: Subset1Component;
  let fixture: ComponentFixture<Subset1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subset1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Subset1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
