import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subset2Component } from './subset2.component';

describe('Subset2Component', () => {
  let component: Subset2Component;
  let fixture: ComponentFixture<Subset2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subset2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Subset2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
