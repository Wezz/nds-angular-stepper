# Getting started - Quick guide

For more detailed documentation see - https://docs.google.com/document/d/1DkHm6moAdi9fsKZSlt-X_uTaEMO3X9d05Sk-Iu_fRjA/edit?usp=sharing

To setup a new Angular project using the Angular CLI

        npm install -g @angular/cli
        ng new my-dream-app
        cd my-dream-app
        ng serve

More information here - https://cli.angular.io/        
        
        

## Step 1 - Node modules
First thing you will need to do is install the **Stepper node module** and other UI modules which are dependencies.

    npm i nedbankStepper

 Install locally via zip file -  npm i dist/stepper/stepper-0.0.1.tgz

**The stepper is also dependent on other UI components:**

Modal - npm i nedbankModal - locally via zip  npm i dist/modal/modal-0.0.1.tgz

Button - npm i nedbankButton - locally via zip  npm i dist/button/button-0.0.1.tgz

Tooltip - npm i nedbankTooltip - locally via zip  npm i dist/tooltip/tooltip-0.0.1.tgz


### Import into main ngModule

    import { StepperComponent } from 'stepper';
    import { ButtonComponent } from 'button';
    import { InputComponent } from 'input';
    import { TooltipComponent } from 'tooltip';
    import { ModalComponent } from 'modal';

And of course reference them in the declarations

     declarations: [
        AppComponent,
        StepperComponent,
        ButtonComponent,
        InputComponent,
        TooltipComponent,
        ModalComponent,
      ]

## Step 2 - Create stepper template
Create a stepper template to house your stepper UI component and content area with nested router-outlet (currentStep).

If you are using Angular cli you can use :
   `ng generate component <name of stepper>`
   
**You will now have 3 files to edit**

    <name of stepper>.component.html
    <name of stepper>.component.scss
    <name of stepper>.component.ts

This name should be what the stepper will be used for example if it is for a personal loan application it could be something like “Personal-loan-stepper”.

### Your html file should be:

    <ui-stepper 
        [isShowStepper]="show" [modalSave]="saveModalShown"  
        [modalCancel]="cancelModalShown" (updateValue)="show = $event" 
        (getSave)="saveModalShown = $event" (getCancel)="cancelModalShown = $event"  
        [listItems]="listItems">
    </ui-stepper>

    <div *ngIf="saveModalShown">
        <ui-modal 
            [isShowModal]="show" (updateValue)="show = $event"
            [isGetSave]="saveModalShown"
            (getSave)="saveModalShown = $event"
            modalId="Save"
            [hasIllustration]="false" 
            [modalIllustrationUrl]="false" 
            modalHeading="Would you like to save?" 
            modalText="By saving this form you have the chance to complete it at a later." 
            [hideTooltip]="true"
            buttonText="Save and continue" 
            secondaryButtonText="Save and exit">
        </ui-modal>
    </div>
    <div *ngIf="cancelModalShown"> 
        <ui-modal 
            [isShowModal]="show" (updateValue)="show = $event"
            [isGetCancel]="cancelModalShown"
            (getCancel)="cancelModalShown = $event"
            modalId="Cancel"
            [hasIllustration]="false" 
            [modalIllustrationUrl]="false" 
            modalHeading="Would you like to cancel?" 
            modalText="By cancelling this form you have no chance of completing it." 
            [hideTooltip]="true"
            buttonText="Yes" 
            secondaryButtonText="No">
        </ui-modal>
    </div>


    <div class="nlsg-c-stepper__stepper-content">
        <router-outlet name="currentStep"></router-outlet>
    </div>
      
### Your .ts file should be:

    import { Component, OnInit} from '@angular/core';

    @Component({
      selector: 'app-stepper-template',
      templateUrl: './stepper-template.component.html',
      styleUrls: ['./stepper-template.component.scss']
    })
    export class StepperTemplateComponent implements OnInit {
    
      show: boolean;
      saveModalShown: boolean;
      cancelModalShown: boolean;
    
      constructor() {}
    
      listItems: any = [
      { mainStep: 'Step 1',
      index: 1,
      route: 'sub-set-1',
        sublist: [
            { substep: 'Sub set 1',
              route: 'sub-set-1',
              sublistItem: [
                { subsubstep: 'Sub-Sub set 1',  route: 'sub-sub-set-1'},
              ],
            }
        ],
      },
      { mainStep: 'Step 2',
        index: 2,
        route: 'sub-set-2',
        sublist: [
            { substep: 'Sub set 2',
              route: 'sub-set-2',
              sublistItem: [
                { subsubstep: 'Sub-Sub set 2' ,  route: 'sub-sub-set-2'},
              ],
            },
        ],
      },
      { mainStep: 'Step 3',
        index: 3,
        route: 'sub-set-3',
        sublist: [
            { substep: 'Sub set 3',
              route: 'sub-set-3',
              sublistItem: [
                { subsubstep: 'Sub-Sub set 3' ,  route: 'sub-sub-set-3'},
              ],
            },
        ],
      },
      { mainStep: 'Step 4',
        index: 4
      }
      ];
    
      ngOnInit() {}
    }
    
The **listItems** variable is what the stepper will use to generate the side navigation links and the counter in the top navigation.

**Important note** - **The routes set in this file need to match the actual routes set in the app-routing-module. The nesting of the items is limited to 3 levels deep and the values can be changed but the naming of the items needs to remain the same**

### Your scss file should be:

    @import "../../styles.scss";

    .nlsg-c-stepper__stepper-content {
        width: 100%;
        height: 800px;
        margin-top: 30px;
            padding: 0 30px 0 400px;

    @media screen and (max-width: $smartphone) {
            padding: 0 20px 0 20px;  
        }	
    
        h1 {
            font-family: 'FFMarkWebProMedium', Helvetica, sans-serif;
        }
        p {
            font-family: 'FFMarkWebProRegular', Helvetica, sans-serif;
        }
    }
    
**@import "../../styles.scss";** is the global styling colours, fonts ect.

You can find the partials folder in the repo in the src > assets > partials 

**styles.scss**

        /* Imports */
        
        @import 'assets/partials/grid.scss';
        @import 'assets/partials/responsive.scss';
        @import 'assets/partials/spacing.scss';
        @import 'assets/partials/mixins.scss';
        @import 'assets/partials/colors.scss';
        @import 'assets/partials/icons.scss';
        @import 'assets/partials/fonts.scss';

## Step 3 - Create individual steps

Create each of your steps as separate components.
It is a good idea to map out what steps are required and to add them to the routes (In step 4)
and json structure to avoid confusion and maintain the overall order.


    ng generate component <name of step>
    
Again you will need to edit the html and ts files.


### Your html file should be:

This can consist of whatever you need the page to look like but the **next button needs to look like the following**:

        <ui-button [routerLink]="['/stepper-template', {outlets: {'currentStep': ['sub-set-2']}}]" [routerLinkActive]="['active']" 
            (click)="completedStep(); stepperItem();"
            buttonText="Next to sub set 2"
            [tooltipText]="tooltipText" [tooltipHeadingText]="tooltipHeadingText"
            [hasHeading]="hasHeading" hideTooltip="true"
            [bottomCenter]="bottomCenter" [topCenter]="topCenter">
        </ui-button>
        
        
**Important note** -

**1. The button should have a routerlink which points to the next step.**        
**2. It needs to have the click functions :**
**completedStep() stepperItem()** you can replace this with a function you need for the current page,
as long as they are fired afterwards in your ts file.


### Your steps .ts file should be:

        //Function that runs on click to set sessionstorage values and mark them as completed
         completedStep() {
            setTimeout(() => {
              const addCompleted = document.querySelectorAll('.nlsg-c-list-stepper__ol__li');
              [].forEach.call(addCompleted, (el, i) => {
                if (el.classList.contains('active')) {
                  el.classList.add('step' + i);
        
                  // add your own key here this we need to be specific
                  sessionStorage.step1 = 'completedStep' + i;
        
                  el.classList.add('completed');
                }
              });
        
              // Update Counter in top nav on click
              const getCurrentStep = document.querySelector('.nlsg-c-list-stepper__ol__li.active');
              const currentStepContainer = document.getElementById('current-step');
              const currentStep = getCurrentStep.getAttribute('data-index');
              const total = parseFloat(currentStep) + 1;
              currentStepContainer.innerText = total.toString();
            }, 10);
          }
        
          // Find the last active item in the side stepper and set height to the progress bar and save this in session storage.
        
          stepperItem() {
            setTimeout(() => {
              const allActive = document.querySelectorAll('.active');
              [].forEach.call(allActive, (el) => {
                  const last = allActive[(allActive.length - 1)];
                  const total = last.getBoundingClientRect().top + last.getBoundingClientRect().height - 130;
                  const progress: HTMLElement = document.getElementsByClassName('progress')[0] as HTMLElement;
                  progress.style.height =  total  + 'px';
                  sessionStorage.currentprogress = total;
              }, 100);
            });
          }
          
This is where you should update the **key** to what exactly this step is called.
                  
sessionStorage.`stepName` = 'completedStep' + i; 

## Step 4  - Routing

Create your routes as usual in your routing module remembering that the routes need to match the routes set in the json structure we created earlier. (You can create your routes first and change the values in the listItems[ ] if that is easier)

        import { NgModule } from '@angular/core';
        import { Routes, RouterModule } from '@angular/router';
        
        //Stepper template you created
        import { StepperTemplateComponent } from './stepper-template/stepper-template.component';
        
        //All steps you need in your stepper go here
        import { Subset1Component } from './subset1/subset1.component';
        import { Subsubset1Component } from './subsubset1/subsubset1.component';
        
        const appRoutes: Routes = [
           //All the routes in your app will be on the same level as stepper template 
            { path: 'page-before-stepper-template', component: StepperTemplateComponentBefore };
            { path: 'stepper-template', component: StepperTemplateComponent,
                children: [
                  {
                    path: 'sub-set-1',
                    component: Subset1Component,
                    outlet: 'currentStep'
                  },
                  {
                    path: 'sub-sub-set-1',
                    component: Subsubset1Component,
                    outlet: 'currentStep'
                  }
                ]
          }
        ];
        @NgModule({
          imports: [
            RouterModule.forRoot(
              appRoutes, { enableTracing: false } // <-- debugging purposes only
            )
          ],
          exports: [RouterModule]
        })
        export class AppRoutingModule { }
        
All the routes in your stepper will be children of stepper template and loaded in the named outlet called **‘currentStep'.**

The stepper leverages the **current active route to show active link styling** and when it should be disabled as well as where the progress bar should be at any time during a flow. 



